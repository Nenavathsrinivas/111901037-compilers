structure IR : sig
    type inst = (string, Temp.temp) MIPS.inst
    type stmt = (string, Temp.temp) MIPS.stmt
    type prog = stmt list
    val ppInst : stmt -> string
    val ppStmt : stmt -> string
    val pp     : prog -> string
end = struct
    type inst = (string, Temp.temp) MIPS.inst
    type stmt = (string, Temp.temp) MIPS.stmt
    type prog = stmt list
    
    fun ppInst (MIPS.ADD (a, b, c)) = "ADD " ^ Temp.tempToString a ^ ", " ^ Temp.tempToString b ^ ", " ^ Temp.tempToString c
        | ppInst (MIPS.MUL (a, b, c)) = "MUL " ^ Temp.tempToString a ^ ", " ^ Temp.tempToString b ^ ", " ^ Temp.tempToString c
        | ppInst (MIPS.OR (a, b, c)) = "OR " ^ Temp.tempToString a ^ ", " ^ Temp.tempToString b ^ ", " ^ Temp.tempToString c
        | ppInst (_) = "not valid"

    fun ppStmt(x) = ppInst(x)   

    fun pp [] = ""
        | pp (x :: xs) = (ppStmt x) ^ pp xs
end