signature inst = sig
    type t
    val isjumplike : t -> bool
    val istarget : t -> bool
end

structure mipsinst : inst = struct

	type t = (string, mips.reg) mips.stmt

	fun isjumplike (mips.dir( _ )) = false
		| isjumplike (mips.label( _ )) = false
		| isjumplike (mips.inst(_)) = true

	fun istarget (mips.label( _ )) = true
		| istarget _ = false

end

functor basicblocks (i : inst) = struct
	structure inst = i 
	type block = i.t list

	fun basicblocks _ = ( _ , [] , [] )

	fun prblocks [] = ""
		| prblocks(x::xs) = (mips.program x) ^ "\n" ^ ( prblocks xs)

end

structure mipsbasicblocks = basicblocks (mipsinst)