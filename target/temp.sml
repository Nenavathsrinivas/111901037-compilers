signature TEMP =
   sig
      type temp
      type label
      val newtemp  : unit -> temp
      val newlabel : unit -> label
      val tempToString : temp -> string
      val labelToString : temp -> string
   end
structure Temp : TEMP = 
   struct
      type temp  = int
      type label = int
      val nxtT  = ref 0
      val nxtL = ref 0
      fun newtemp _ = ( nxtT := !nxtT + 1; !nxtT )
      fun newlabel _ = (nxtL := !nxtL + 1; !nxtL)
      fun tempToString t = "t" ^ Int.toString(!nxtT)
      fun labelToString l = "l" ^ Int.toString(l)
end
