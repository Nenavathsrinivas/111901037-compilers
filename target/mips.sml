structure MIPS = struct

datatype regs = zero | at | v0 | v1 | a0 | a1 | a2 | a3 
		| t0 | t1 | t2 | t3 | t4 | t5 | t6 | t7 
		| s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7
		| t8 | t9 | k0 | k1 | gp | sp | fp | ra
						     
datatype ('l,'t) inst =
                LA of 't * int    
	          | LB of 't * int    
		      | LBU of 't * int   
		      | LD of 't * int    
		      | LH of 't * int    
		      | LHU of 't * int   
		      | LW of 't * int    
		      | LWCZ of 't * int  
		      | LWL of 't * int   
		      | LWR of 't * int   
		      | SB of 't * int    
		      | SD of 't * int    
		      | SH of 't * int    
		      | SW of 't * int    
		      | SWCZ of 't * int  
		      | SWL of 't * int   
		      | SWR of 't * int   
		      | ULH of 't * int   
		      | ULHU of 't * int  
		      | ULW of 't * int   
		      | USH of 't * int   
		      | USW of 't * int   
	 
	 
	          | RFE            
		      | SYSCALL            
		      | BREAK              
		      | NOP                
			    
	 
	              | LI  of 't * int      
		      | LUI of 't * int          
					
	 
	              | ABS of 't * 't      
		      | ADD of 't * 't * 't     
		      | ADDI of 't * 't * int   
		      | ADDU of 't * 't * 't    
		      | ADDIU of 't * 't * int  
		      | AND of 't * 't * 't     
		      | ANDI of 't * 't * int	
		      | DIVI of 't * 't         
		      | DIVIU of 't * 't        
		      | DIV of 't * 't * 't     
		      | DIVU of 't * 't * 't    
		      | MUL of 't * 't * 't     
		      | MULO of 't * 't * 't    
		      | MULOU of 't * 't * 't   
		      | MULT of 't * 't         
		      | MULTU of 't * 't        
		      | NEG of 't * 't          
		      | NEGU of 't * 't			
		      | NOR of 't * 't * 't     
		      | NOT of 't * 't          
		      | OR of 't * 't * 't      
		      | ORI of 't * 't * int    
		      | REM of 't * 't * 't     
		      | REMU of 't * 't * 't    
		      | ROL of 't * 't * 't     
		      | ROR of 't * 't * 't     
		      | SLL of 't * 't * 't     
		      | SLLV of 't * 't * 't    
		      | SRA of 't * 't * 't     
		      | SRAV of 't * 't * 't    
		      | SRL of 't * 't * 't     
		      | SRLV of 't * 't * 't    
		      | SUB of 't * 't * 't     
		      | SUBU of 't * 't * 't    
		      | XOR of 't * 't * 't     
		      | XORI of 't * 't * int   
					     
					     
		      | SEQ of 't * 't * 't       
		      | SGE of 't * 't * 't       
		      | SGEU of 't * 't * 't      
		      | SGT of 't * 't * 't       
		      | SGTU of 't * 't * 't      
		      | SLE of 't * 't * 't       
		      | SLEU of 't * 't * 't      
		      | SLT of 't * 't * 't       
		      | SLTI of 't * 't * int     
		      | SLTU of 't * 't * 't      
		      | SLTIU of 't * 't * int    
		      | SNE of 't * 't * 't       
					     
					     
		      | B of 'l                       
		      | BCZT of 'l                    
		      | BCZF of 'l                    
		      | BEQ of 't * 't * 'l           
		      | BEQZ of 't * 'l               
		      | BGE of 't * 't * 'l           
                      | BGEU of 't * 't * 'l  
		      | BGEZ of 't * 'l               
		      | BGT of 't * 't * 'l           
		      | BGTU of 't * 't * 'l          
		      | BQTZ of 't * 'l               
		      | BLE of 't * 't * 'l           
		      | BLEU of 't * 't * 'l          
 		      | BLEZ of 't * 'l               
		      | BGEZAL of 't * 'l            
		      | BLTZAL of 't * 'l             
		      | BLT of 't * 't * 'l           
		      | BLTU of 't * 't * 'l          
		      | BLTZ of 't * 'l               
		      | BNE of 't * 't * 'l           
		      | BNEZ of 't * 'l               
		      | J of 'l                       
		      | JAL of 'l                     
		      | JALR of 't                    
		      | JR of 't                      


		      | MOVE of 't * 't            
		      | MFHI of 't                 
		      | MFLO of 't                 
		      | MTHI of 't                 
		      | MTLO of 't                 
		      | MFCZ of 't * 't            
		      | MFC1_D of 't * 't          
		      | MTCZ of 't * 't            
		      
datatype ('l,'t) stmt = Inst of ('l,'t) inst
			 |ALIGN of int
			 |ASCII of string
			 |ASCIIZ of string
			 |BYTE of int list
			 |DATA of int
			 |FLOAT of int list
			 |DOUBLE of int list
			 |EXTERN of int*int
			 |GLOBL of string
			 |HALF of int list
			 |KDATA of int
			 |KTEXT of int
			 |SPACE of int
			 |TEXT of int
			 |WORD of int list
			 
fun printreg zero = "zero"
    |printreg at   = "at"
    |printreg v0   = "v0"
    |printreg v1   = "v1"
    |printreg a0   = "a0"
    |printreg a1   = "a1"
    |printreg a2   = "a2"
    |printreg a3   = "a3"
    |printreg t0   = "t0"
    |printreg t1   = "t1"
    |printreg t2   = "t2"
    |printreg t3   = "t3"
    |printreg t4   = "t4"
    |printreg t5   = "t5"
    |printreg t6   = "t6"
    |printreg t7   = "t7"
    |printreg t8   = "t8"
    |printreg t9   = "t9"
    |printreg s0   = "s0"
    |printreg s1   = "s1"
    |printreg s2   = "s2"
    |printreg s3   = "s3"
    |printreg s4   = "s4"
    |printreg s5   = "s5"
    |printreg s6   = "s6"
    |printreg s7   = "s7"
    |printreg k0   = "k0"
    |printreg k1   = "k1"
    |printreg gp   = "gp"
    |printreg sp   = "sp"
    |printreg fp   = "fp"
    |printreg ra   = "ra"

fun prInst (ABS (x , y) ) = "ABS" ^ printreg x ^ "," ^printreg y
	| prInst (ADD (x , y, z)) = "add" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (ADDI (x ,y, z)) = "addi" ^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
	| prInst (ADDU (x , y , z)) = "addu"^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
 	| prInst (ADDIU (x , y , z)) = "addiu" ^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
 	| prInst (AND (x , y , z)) = "and"^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (ANDI (x , y , z)) = "andi"^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
	| prInst (DIVI (x , y) ) = "divi" ^ printreg x ^ "," ^ printreg y
	| prInst (DIVIU (x , y) ) = "diviu" ^ printreg x ^ "," ^ printreg y
	| prInst (DIV (x , y, z)) = "div" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (DIVU (x , y, z)) = "divu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (MUL (x , y, z)) = "mul" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	|  prInst (MULO (x , y, z)) = "mulo" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (MULOU (x , y, z)) = "mulou" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (MULT (x , y) ) = "mult" ^ printreg x ^ "," ^ printreg y
	| prInst (MULTU (x , y) ) = "multu" ^ printreg x ^ "," ^ printreg y
	| prInst (NEG (x , y) ) = "neg" ^ printreg x ^ "," ^ printreg y
	| prInst (NEGU (x , y) ) = "negu" ^ printreg x ^ "," ^ printreg y
	| prInst (NOR (x , y, z)) = "nor" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (NOT (x , y) ) = "not" ^ printreg x ^ "," ^ printreg y
	| prInst (OR (x , y, z)) = "or" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (ORI (x , y, z)) = "ori" ^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
	| prInst (REM (x , y, z)) = "rem" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (REMU (x , y, z)) = "remu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (ROL (x , y, z)) = "rol" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (ROR (x , y, z)) = "ror" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLL (x , y, z)) = "sll" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLLV (x , y, z)) = "sllv" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SRA (x , y, z)) = "sra" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SRAV (x , y, z)) = "srav" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SRL (x , y, z)) = "srl" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SRLV (x , y, z)) = "srlv" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SUB (x , y, z)) = "sub" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SUBU (x , y, z)) = "subu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (XOR (x , y, z)) = "xor" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (XORI (x , y, z)) = "xori" ^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
	| prInst (SEQ (x , y, z)) = "seq" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SGE (x , y, z)) = "sge" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SGEU (x , y, z)) = "sgeu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SGT (x , y, z)) = "sgt" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SGTU (x , y, z)) = "sgtu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLE (x , y, z)) = "sle" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLEU (x , y, z)) = "sleu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLT (x , y, z)) = "slt" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLTI (x , y, z)) = "slti" ^ printreg x ^ "," ^ printreg y ^ "," ^ Int.toString z
	| prInst (SLTU (x , y, z)) = "sltu" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (SLTIU (x , y, z)) = "sltiu" ^ printreg x ^ "," ^ printreg y ^ "," ^  Int.toString z
	| prInst (SNE (x , y, z)) = "SNE" ^ printreg x ^ "," ^ printreg y ^ "," ^ printreg z
	| prInst (MOVE (x , y)) = "MOVE" ^ printreg x ^ "," ^ printreg y
	| prInst (MFHI (x)) = "MFHI" ^ printreg x
	| prInst (MFLO (x)) = "MFLO" ^ printreg x
	| prInst (MTHI (x)) = "MTHI" ^ printreg x
	| prInst (MTLO (x)) = "MTLO" ^ printreg x
	| prInst (MFCZ (x , y)) = "MFCZ" ^ printreg x ^ "," ^ printreg y
	| prInst (MFC1_D (x , y)) = "MFCZ_D" ^ printreg x ^ "," ^ printreg y
	| prInst (MTCZ (x , y)) = "MTCZ" ^ printreg x ^ "," ^ printreg y
	| prInst (LA (x , y)) = "LA" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LB (x , y)) = "LB" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LBU (x , y)) = "LBU" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LD (x , y)) = "LD" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LH (x , y)) = "LH" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LHU (x , y)) = "LHU" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LW (x , y)) = "LW" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LWCZ (x , y)) = "LWCZ" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LWL (x , y)) = "LWL" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LWR (x , y)) = "LWR" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SB (x , y)) = "SB" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SD (x , y)) = "SD" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SH (x , y)) = "SH" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SW (x , y)) = "SW" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SWCZ (x , y)) = "SWCZ" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SWL (x , y)) = "SWL" ^ printreg x ^ "," ^ Int.toString y
	| prInst (SWR (x , y)) = "SWR" ^ printreg x ^ "," ^ Int.toString y
   	| prInst (ULH (x , y)) = "ULH" ^ printreg x ^ "," ^ Int.toString y
   	| prInst (ULHU (x , y)) = "ULHU" ^ printreg x ^ "," ^ Int.toString y
	| prInst (ULW (x , y)) = "ULW" ^ printreg x ^ "," ^ Int.toString y
	| prInst (USH (x , y)) = "USH" ^ printreg x ^ "," ^ Int.toString y
	| prInst (USW (x , y)) = "USW" ^ printreg x ^ "," ^ Int.toString y
	| prInst RFE = "RFE"
	| prInst SYSCALL = "SYSCALL"
	| prInst BREAK = "BREAK"
	| prInst NOP = "NOP"
	| prInst (LI (x , y)) = "LI" ^ printreg x ^ "," ^ Int.toString y
	| prInst (LUI (x , y)) = "LUI" ^ printreg x ^ "," ^ Int.toString y
	| prInst (B (x)) = "B" ^ x
	| prInst (BCZT (x)) = "BCZT" ^ x
	| prInst (BCZF (x)) = "BCZF" ^ x
	| prInst (BEQ (x , y, z)) = "BEQ" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BEQZ (x , y)) = "BEQZ" ^ printreg x ^ "," ^ y
	| prInst (BGE (x , y, z)) = "BGE" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BGEU (x , y, z)) = "BGEU" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BGEZ (x , y)) = "BGEZ" ^ printreg x ^ "," ^ y
	| prInst (BGT (x , y, z)) = "BGT" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BGTU (x , y, z)) = "BGTU" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BQTZ (x , y)) = "BQTZ" ^ printreg x ^ "," ^ y
	| prInst (BLE (x , y, z)) = "BLE" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BLEU (x , y, z)) = "BLEU" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BLEZ (x , y)) = "BLEZ" ^ printreg x ^ "," ^ y
	| prInst (BGEZAL (x , y)) = "BGEZAL" ^ printreg x ^ "," ^ y
	| prInst (BLTZAL (x , y)) = "BLTZAL" ^ printreg x ^ "," ^ y
	| prInst (BLT (x , y, z)) = "BLT" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BLTU (x , y, z)) = "BLTU" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BLTZ (x , y)) = "BLTZ" ^ printreg x ^ "," ^ y
	| prInst (BNE (x , y, z)) = "BNE" ^ printreg x ^ "," ^ printreg y ^ "," ^ z
	| prInst (BNEZ (x , y)) = "BNEZ" ^ printreg x ^ "," ^ y
	| prInst (J x) = "J" ^ x
	| prInst (JAL x) = "JAL" ^ x
	| prInst (JALR x) = "JALR" ^ printreg x
	| prInst (JR x ) = "JR" ^ printreg x
	
fun printlist (x :: xs) = Int.toString x ^ ", " ^ printlist xs
    |printlist _         = ""
	
fun prStmt (Inst x) = prInst x ^ "\n"
    |prStmt (ALIGN x) = ".align" ^ Int.toString x
    |prStmt (ASCII x) = ".ascii" ^ x
    |prStmt (ASCIIZ x) = ".asciiz" ^ x
    |prStmt (DATA x) = ".data" ^ Int.toString x
    |prStmt (FLOAT x) = ".float " ^ printlist x
    |prStmt (BYTE x)= ".byte " ^ printlist x
    |prStmt (WORD x) = ".word" ^ printlist x
    |prStmt (DOUBLE x) = ".double" ^ printlist x
    |prStmt (GLOBL x) = ".globl" ^ x
    |prStmt (HALF x) = ".half" ^ printlist x
    |prStmt (KDATA x) =".kdata" ^ Int.toString x
    |prStmt (KTEXT x) =".ktext" ^ Int.toString x
    |prStmt (SPACE x) =".space" ^ Int.toString x
    |prStmt (TEXT x) =".text" ^ Int.toString x
    |prStmt (EXTERN (x,y)) =".extern" ^ Int.toString x  ^ Int.toString y

end