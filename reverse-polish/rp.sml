
structure RP =
struct
val interactive = RPLex.makeLexer (fn _ => TextIO.inputN (TextIO.stdIn,1))

fun lexfile file = let val strm = TextIO.openIn file
		   in RPLex.makeLexer (fn n => TextIO.inputN(strm,n))
		   end

fun runWithLexer lexer = let fun loop stack = case lexer () of
						  NONE      => ()
					       |  SOME inst => loop (Machine.step inst stack)
			 in loop []
			 end

val _ =  ( case CommandLine.arguments() of
	       [] => runWithLexer interactive
	    |  xs => (List.map (runWithLexer o lexfile) xs; ())
	 )
	 handle Machine.StackUnderflow s =>
		( print "error: Stack underflow: " ;
		  Machine.printstack s;
		  OS.Process.exit OS.Process.failure
		)

end
